/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.learning.metric.entropy;



import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.action.AddLinkEdit;
import org.openmarkov.core.action.InvertLinkEdit;
import org.openmarkov.core.action.RemoveLinkEdit;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.io.database.elvira.ElviraDataBaseIO;


public class EntropyMetricTest {
	
	ProbNet probNet;
	
	EntropyMetric metric;
	
	private String dbFilename = "/learnTestDataBase.dbc";
	
    private final double maxError                  = 1E-6;
    private final double expectedScore             = -3981.22537135;
    private final double expectedScoreLinkAdded    = 260.68843349;
    private final double expectedScoreLinkInverted = -1.136868e-13;
    private final double expectedScoreLinkRemoved  = -260.68843349;
	
	@Before
	public void setUp() throws Exception {
        ElviraDataBaseIO databaseIO = new ElviraDataBaseIO ();
        CaseDatabase database = databaseIO.load(getClass().getResource (dbFilename).getFile ());
        probNet = new ProbNet ();
        
        for(Variable variable: database.getVariables ())
        {
            probNet.addNode (variable, NodeType.CHANCE);
        }        

        ArrayList<Integer> variableIndex = new ArrayList<Integer>();
        for (int i = 0; i < probNet.getNumNodes(); i++){
            variableIndex.add(new Integer(i));
        }
        
        metric = new EntropyMetric();
        metric.init (probNet, database);
	}

	@Test
	public void testScores() {
	    assertEquals(expectedScore, metric.getScore(), maxError);	    
	}

	@Test
	public void testScoreLinkAdded() throws  
			NodeNotFoundException {
		AddLinkEdit edition = new AddLinkEdit(probNet, 
				probNet.getVariable("E"), probNet.getVariable("D"), true);
		assertEquals(expectedScoreLinkAdded, metric.getScore(edition), maxError);
	}

	@Test
	public void testScoreLinkInverted() throws Exception {
		AddLinkEdit auxiliarEdition = new AddLinkEdit(probNet,
				probNet.getVariable("E"), probNet.getVariable("D"), true);

		probNet.doEdit(auxiliarEdition);
        
		InvertLinkEdit edition = new InvertLinkEdit(probNet, 
				probNet.getVariable("E"), probNet.getVariable("D"), true);
		assertEquals(expectedScoreLinkInverted, metric.getScore(edition), maxError);
	}
	
	@Test
	public void testScoreLinkRemoved() throws Exception {
		AddLinkEdit auxiliarEdition = new AddLinkEdit(probNet, 
				probNet.getVariable("D"), probNet.getVariable("E"), true);
		
		probNet.doEdit(auxiliarEdition);
        
		RemoveLinkEdit edition = new RemoveLinkEdit(probNet, 
				probNet.getVariable("D"), probNet.getVariable("E"), true);
		assertEquals(expectedScoreLinkRemoved, metric.getScore(edition), maxError);
	}
}
